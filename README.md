# LinOT for Python Library

## Overview
This library provides a basic python implementation for linearized optimal transport data analysis, covering various transport metrics and special cases.
The code is supposed to be organized approximately into three layers:
* Basic routines for solving optimal transport problems, and for performing the barycentric projection approximation of the logarithmic map. For large problems, specialized more powerful solvers may be necessary.
* Embedding classes that manage tangent vectors at a given reference measure, convert them to a Euclidean basis and provide a convenient interface for dimensionality reduction, analysis in tangent space, and shooting back to the measure manifold. This class is first implemented for W2 in more than one dimension, and then specialized to various other scenarios, including
  * Gaussian measures
  * one-dimensional W2 analysis
  * unbalanced HK and SHK metrics.
* There should be a large collection of visualization routines that cover common workflows. Roughly, visualization routines can be grouped into two classes: low- and high-level, where the former add smaller primitives to a figure and the latter usually take an instance of an embedding class as argument and produce a whole complex figure.

The library is maintained by the [Optimal Transport Group at Göttingen University](https://ot.cs.uni-goettingen.de/), the original authors are Jan Niklas Dühmert, Clément Sarrazin, and Bernhard Schmitzer.

## Installation
You can either use the code from this repository or install the library via pip by running
```
pip install LinOT
```
If the LinOT package is installed via pip, it is no longer necessary to add the lib directory of this repository to the python import path, as done in the example scripts below.

## Getting started
* Various examples are given in [python/examples](python/examples).
 * These cover solving (unbalanced) entropic transport problems with a basic implementation of the Sinkhorn algorithm, and subsequent extraction of approximate tangent vectors via barycentric projection.
 * Analysis of small datasets in tangent space, in particular via PCA, with various suggestions for visualization.
 * Special cases of the linearized W2 distance: Gaussian measures and the one-dimensional setting.
* We encourage you to work with this library on your datasets and let us know if useful functionality is missing.

## Literature
For background, more relevant literature, and mathematical details see the following to references:
* Tianji Cai, Junyi Cheng, Bernhard Schmitzer, Matthew Thorpe: The Linearized Hellinger-Kantorovich Distance, SIAM Journal on Imaging Sciences 15(1), 45-83 (2022), available at https://doi.org/10.1137/21M1400080 or https://arxiv.org/abs/2102.08807
* Clément Sarrazin, Bernhard Schmitzer: Linearized optimal transport on manifolds, 2023, https://arxiv.org/abs/2303.13901

## License (MIT License)
Copyright (c) 2024, Jan Niklas Dühmert, Clément Sarrazin, Bernhard Schmitzer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
