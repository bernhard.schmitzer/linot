# ---
# jupyter:
#   jupytext:
#     formats: py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import numpy as np
import scipy
import scipy.interpolate

import lib.LinOT as LinOT
import lib.LinOT.Visualization as Vis

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
# -

from lib.LinOT import *

# # Experiment with right type of inverse interpolation

nSamples=10
nPoints=10
xList=np.random.normal(size=(nSamples,nPoints))
xList=np.sort(xList,axis=1)
mList=np.random.random(size=(nSamples,nPoints))
mList=np.einsum(mList,[0,1],1/np.sum(mList,axis=1),[0],[0,1])

i=0
x=xList[i]
m=mList[i]
mcum=np.cumsum(m)

mcum

# ## Atomic measures: T is piecewise constant

mcumHat=np.concatenate((np.array([0.]),mcum))
xHat=np.concatenate((x[[0]],x))
print(mcumHat)
print(xHat)

plt.step(mcumHat,xHat,where="pre")
#plt.step(mcum,x,where="post")
plt.show()



f=scipy.interpolate.interp1d(mcumHat,xHat,kind="next",assume_sorted=True)

num=1000
y=np.linspace(0,1,num=num,endpoint=False)

f(0.99)

plt.step(mcumHat,xHat,where="pre")
plt.plot(y,f(y))
plt.show()

# ## Piecewise constant density: T is piecewise linear

# in this setting assume each mass is mass in a histogram bin of fixed width,
# with various bin specs that can be provided in the constructor
# for now: just give n+1 positions that are start of first bin up to end of last bin
xHat=np.linspace(-1,1,num=nPoints+1)

mcumHat

plt.plot(xHat,mcumHat,marker="x")
plt.show()

plt.plot(mcumHat,xHat,marker="x")
plt.show()

f=scipy.interpolate.interp1d(mcumHat,xHat,kind="linear",assume_sorted=True)

plt.plot(mcumHat,xHat,marker="x")
plt.plot(y,f(y))
plt.show()


# # Actual classes

class LinW2_1dHistEmbedding(LinW2Embedding):
    def __init__(self,nPts,bins):
        self.nPts=nPts
        self.bins=bins
        self.nBins=bins.shape[0]-1
        self.pos=np.linspace(0,1,num=self.nPts,endpoint=False)
        self.samples=np.zeros((0,self.nPts))
        self.mean=np.zeros((self.nPts,))
        self.Exp = lambda x: x

    def convertRawToEuclideanSingle(self,mass,shiftMean=True):
        massCum=np.cumsum(mass)
        massCumHat=np.concatenate((np.array([0.]),massCum))
        f=scipy.interpolate.interp1d(massCumHat,self.bins,kind="linear",assume_sorted=True)
        return f(self.pos)
        
    def convertRawToEuclidean(self,samples,shiftMean=True):
        """covs is a single or a list of covariance matrices
        means is a single or a list of mean vectors"""
        if samples.ndim==1:
            ## single sample
            result=self.convertRawToEuclideanSingle(samples,shiftMean=shiftMean)
            if shiftMean:
                result-=self.mean
        elif samples.ndim==2:
            ## list of samples
            nsmp=samples.shape[0]
            result=np.zeros((nsmp,self.nPts),dtype=np.double)
            for i in range(nsmp):
                result[i]=self.convertRawToEuclideanSingle(samples[i],shiftMean=shiftMean)
            if shiftMean:
                result-=self.mean.reshape((1,-1))
        else:
            raise ValueError("wrong dimension")
        return result
    
    def convertEuclideanToRaw(self,samples,shiftMean=True):
        if samples.ndim==1:
            ## single sample
            pos=samples.copy()
            if shiftMean:
                pos+=self.mean
            #return pos,np.full((self.nPts,),fill_value=1./self.nPts)
            res=np.histogram(pos,bins)
            return res[0]/self.nPts
        elif samples.ndim==2:
            ## multiple samples
            nsmp=samples.shape[0]
            pos=samples.copy()
            if shiftMean:
                pos+=self.mean
            result=np.zeros((nsmp,self.nBins),dtype=np.double)
            for i in range(nsmp):
                result[i]=np.histogram(pos[i],bins)[0]/self.nPts
            return result
        else:
            raise ValueError("wrong dimension")


# # Testing usage of class

nSamples=1000
nPoints=10
means=np.random.random(size=(nSamples,))
std=0.5+np.random.random(size=(nSamples,))
xList=np.array([np.random.normal(loc=mean,scale=std,size=(nPoints,)) for mean,std in zip(means,std)])
xList=np.sort(xList,axis=1)
mList=np.random.random(size=(nSamples,nPoints))
mList=np.einsum(mList,[0,1],1/np.sum(mList,axis=1),[0],[0,1])

Embedding=LinW2_1dEmbedding(1000)
Embedding.addSamples((xList,mList))
Embedding.centerSamples()

# +
for i in range(len(xList)):
    dat=Embedding.convertEuclideanToRaw(Embedding.samples[i])
    dat[1][...]=np.cumsum(dat[1])
    plt.plot(*dat,c=colors[0])

dat=Embedding.getRawMean()
dat[1][...]=np.cumsum(dat[1])
plt.plot(*dat,c=colors[1])
    
plt.show()

# +
for i in range(len(xList)):
    plt.plot(Embedding.pos,Embedding.mean+Embedding.samples[i],c=colors[0])

plt.plot(Embedding.pos,Embedding.mean,c=colors[1])
    
plt.show()
# -

Embedding.performPCA()

plt.plot(Embedding.pca_std,marker="x")
plt.show()

plt.scatter(Embedding.pca_coords[:,0],Embedding.pca_coords[:,1])
plt.show()

for i in range(3):
    for j in range(len(xList)):
        plt.plot(Embedding.pos,Embedding.mean+Embedding.samples[j],c=cm.gray(0.5))

    plt.plot(Embedding.pos,Embedding.mean,c=colors[1])
    for j,t in enumerate(np.linspace(-1,1,num=7)):
        plt.plot(Embedding.pos,Embedding.mean+t*Embedding.pca_std[i]*Embedding.pca_vec[i],c=cm.viridis(j/6))

    plt.show()

# # Hist class

nPoints=1000
bins=np.arange(nPoints+1)

mList=np.zeros((2,nPoints))
mList[0,0]=1.
mList[1,-1]=1.

EmbeddingHist=LinW2_1dHistEmbedding(1000,bins)
EmbeddingHist.addSamples(mList)
EmbeddingHist.centerSamples()

i=0
j=1
dat0=EmbeddingHist.convertEuclideanToRaw(EmbeddingHist.samples[i])
dat1=EmbeddingHist.convertEuclideanToRaw(EmbeddingHist.samples[j])
datList=[EmbeddingHist.convertEuclideanToRaw((1-t)*EmbeddingHist.samples[i]+t*EmbeddingHist.samples[j])\
        for t in np.linspace(0,1,num=7)]

#plt.step(bins[:-1],mList[i])
#plt.step(bins[:-1],mList[j])
#plt.step(bins[:-1],dat0)
#plt.step(bins[:-1],dat1)
for i in range(len(datList)):
    plt.step(bins[:-1],datList[i],c=cm.viridis(i/6))
plt.show()

Embedding.performPCA()

plt.plot(Embedding.pca_var,marker="x",lw=0)
plt.show()

plt.scatter(Embedding.pca_coords[:,0],Embedding.pca_coords[:,1])
plt.show()


