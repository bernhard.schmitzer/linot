# ---
# jupyter:
#   jupytext:
#     formats: py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import numpy as np
import lib.LinOT.auxiliary as aux
import lib.LinOT.SinkhornNP as Sinkhorn
import auxlib.OTCommon as OTCommon

import lib.LinOT as LinOT
import lib.LinOT.Visualization as Vis

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

# +
tag="ex1_single-ellipses"
filename="examples/data/"+tag+"/tan_data_HK_kappa32.npz"
dat=np.load(filename)
tanDataV=dat["tanListV"]
tanDataAlpha=dat["tanListAlpha"]
del dat

muRef,posRef=OTCommon.importImage("examples/data/"+tag+"/center_data.npz")
kappa=32
# -

Embedding=LinOT.LinHKEmbedding(muRef,posRef,kappa)

Embedding.addSamples((tanDataV,tanDataAlpha))

Embedding.centerSamples()

Embedding.performPCA()
#print(Embedding.pca_std)
plt.plot(Embedding.pca_var,marker="x",lw=0)
plt.show()

plt.scatter(Embedding.pca_coords[:,0],Embedding.pca_coords[:,1])
plt.show()

i=0
res=Embedding.convertEuclideanToRaw(Embedding.samples[i])
print(np.sum((res[0]-tanDataV[i])**2))
print(np.sum((res[1]-tanDataAlpha[i])**2))

i=0
res=Embedding.expEuclidean(Embedding.samples[i])
img=aux.rasterizePointCloud2d(*res,(64,64,))
plt.imshow(img)
plt.show()

for i in [0,1,2]:
    print(i)
    vec=np.zeros(i+1)
    vec[i]=1.
    res=Embedding.expPCA(vec,tSeq=Embedding.pca_std[i]*np.linspace(-1,1,num=7))

    imgList=[aux.rasterizePointCloud2d(mu,pos,(64,64,)) for mu,pos in res]
    Vis.ShowImageArray(imgList,7,2,2,scaleMode="common")

v,alpha=Embedding.convertEuclideanToRaw(Embedding.pca_vec[2],shiftMean=False)

a=np.einsum(v**2,[0,1],muRef,[0],[])
b=kappa**2/4*np.sum(alpha**2*muRef)
print([a**0.5,b**0.5,(a+b)**0.5])

np.sum(Embedding.pca_vec[2]**2)


