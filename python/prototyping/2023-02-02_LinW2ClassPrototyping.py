# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import numpy as np
import lib.LinOT.auxiliary as aux
import lib.LinOT.SinkhornNP as Sinkhorn
import auxlib.OTCommon as OTCommon

import lib.LinOT as LinOT
import lib.LinOT.Visualization as Vis

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

# +
tag="ex1_single-ellipses"
filename="examples/data/"+tag+"/tan_data.npz"
tanData=np.load(filename)["tanList"]

filename="examples/data/"+tag+"/center_data.npz"
imgCenter=np.load(filename)["img"]

totalMass=1.
keepZero=False
muRef,posRef=OTCommon.processDensity_Grid(imgCenter,\
        totalMass=totalMass,keepZero=keepZero)
# -

Embedding=LinOT.LinW2Embedding(muRef,posRef)

Embedding.addSamples(tanData)

Embedding.centerSamples()

Embedding.performPCA()

plt.scatter(Embedding.pca_coords[:,0],Embedding.pca_coords[:,1])
plt.show()

res=Embedding.expEuclidean(None)
img=aux.rasterizePointCloud2d(*res,(64,64,))
plt.imshow(img)
plt.show()

for i in [0,1]:
    print(i)
    vec=np.zeros(i+1)
    vec[i]=1.
    res=Embedding.expPCA(vec,tSeq=Embedding.pca_std[i]*np.linspace(-1,1,num=7))

    imgList=[aux.rasterizePointCloud2d(mu,pos,(64,64,)) for mu,pos in res]
    Vis.ShowImageArray(imgList,7,2,2,scaleMode="common")


