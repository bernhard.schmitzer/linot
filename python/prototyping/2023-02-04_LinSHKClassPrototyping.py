# ---
# jupyter:
#   jupytext:
#     formats: py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import numpy as np
import lib.LinOT.auxiliary as aux
import lib.LinOT.SinkhornNP as Sinkhorn
import auxlib.OTCommon as OTCommon

import lib.LinOT as LinOT
import lib.LinOT.Visualization as Vis

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

# +
#from lib.LinOT import *

# +
tag="ex1_single-ellipses"
filename="examples/data/"+tag+"/tan_data_HK_kappa32.npz"
dat=np.load(filename)
tanDataV=dat["tanListV"]
tanDataAlpha=dat["tanListAlpha"]
del dat

muRef,posRef=OTCommon.importImage("examples/data/"+tag+"/center_data.npz")
kappa=32
# -

if False:
    class LinHKEmbedding(LinW2Embedding):
        def __init__(self,_muRef,_posRef,_kappa,SHK=False,_baseExp=None):
            LinW2Embedding.__init__(self,_muRef,_posRef,_baseExp=_baseExp)
            self.massRef=np.sum(self.muRef)
            self.samples=np.zeros((0,self.nPts*(self.dim+1)))
            self.mean=np.zeros((self.nPts*(self.dim+1),))
            self.kappa=_kappa
            self.Exp=lambda tan : ExpHK(self.muRef,self.posRef,\
                    tan[0],tan[1],self.kappa,self.baseExp)
            # whether to use SHK mode
            self.SHK=SHK
        def getSPrime(self,shk):
            return np.maximum(shk/self.kappa,1E-100)/np.sin(np.maximum(shk/self.kappa,1E-100))

        def normalizeTan(self,v,alpha):
            """Re-scale a tangent vector (v,alpha) such that it maps to the target measure,
            re-scaled such that it has the same mass as the reference measure."""
            # first shoot, to see what the target mass is
            muExp,posExp=ExpHK(self.muRef,self.posRef,\
                    v,alpha,self.kappa,self.baseExp)
            q=(self.massRef/np.sum(muExp))**0.5
            vHat=q*v
            alphaHat=q*alpha+2*(q-1)
            return vHat,alphaHat

        def tanToSHK(self,v,alpha):
            #hk squared / kappa^2: given by mean of alpha, see paper2
            hk2Scaled=-np.sum(alpha*muRef)
            shk=self.kappa*np.arccos(1-hk2Scaled/2)
            # re-scaling
            s=self.getSPrime(shk)
            return (s*v,s*(alpha+hk2Scaled))

        def tanToHK(self,v,alpha):
            shk=(np.einsum(v**2,[0,1],self.muRef,[0],[])+\
                    0.25*self.kappa**2*np.sum(alpha**2*self.muRef))**0.5
            s=self.getSPrime(shk)
            hk2Scaled=2*(1-np.cos(shk/self.kappa))
            return (v/s,alpha/s-hk2Scaled)


        def convertRawToEuclidean(self,samples,shiftMean=True):
            vs,alphas=samples
            if vs.ndim==2:
                ## single sample
                # if SHK mode then translate
                if self.SHK:
                    vs,alphas=self.normalizeTan(vs,alphas)
                    vs,alphas=self.tanToSHK(vs,alphas)
                samp=np.concatenate((vs,0.5*self.kappa*alphas.reshape((-1,1))),axis=1)
                result=np.einsum(samp,[0,1],self.muSqrt,[0],[0,1]).reshape((-1,))
                if shiftMean:
                    result-=self.mean
            elif vs.ndim==3:
                ## list of samples
                nsmp=vs.shape[0]
                if self.SHK:
                    vs=vs.copy()
                    alphas=alphas.copy()
                    for i in range(nsmp):
                        vs[i],alphas[i]=self.normalizeTan(vs[i],alphas[i])
                        vs[i],alphas[i]=self.tanToSHK(vs[i],alphas[i])
                samp=np.concatenate((vs,0.5*self.kappa*alphas.reshape((nsmp,-1,1))),axis=2)
                result=np.einsum(samp,[0,1,2],self.muSqrt,[1],[0,1,2]).reshape((nsmp,-1))
                if shiftMean:
                    result-=self.mean.reshape((1,-1))
            else:
                raise ValueError("wrong dimension")
            return result



        def convertEuclideanToRaw(self,samples,shiftMean=True):
            if (samples is None):
                # just return mean as raw (small short cut to avoid having to pass a zero
                # vector)
                result=np.einsum(self.mean.reshape((self.nPts,self.dim+1)),[0,1],\
                        1/self.muSqrt,[0],[0,1])
                v=result[:,:self.dim]
                alpha=result[:,-1]*2/self.kappa
                if self.SHK:
                    v,alpha=self.tanToHK(v,alpha)
            elif samples.ndim==1:
                ## single sample
                result=samples.copy()
                if shiftMean:
                    result+=self.mean
                result=np.einsum(result.reshape((self.nPts,self.dim+1)),[0,1],\
                        1/self.muSqrt,[0],[0,1])
                v=result[:,:self.dim]
                alpha=result[:,-1]*2/self.kappa
                if self.SHK:
                    v,alpha=self.tanToHK(v,alpha)
            elif samples.ndim==2:
                ## multiple samples
                result=samples.copy()
                if shiftMean:
                    result+=self.mean.reshape((1,-1))
                result=np.einsum(result.reshape((-1,self.nPts,self.dim+1)),[0,1,2],\
                        1/self.muSqrt,[1],[0,1,2])
                v=result[:,:,:self.dim]
                alpha=result[:,:,-1]
                if self.SHK:
                    for i in range(v.shape[0]):
                        v[i],alpha[i]=self.tanToHK(v[i],alpha[i])
            else:
                raise ValueError("wrong dimension")
            return v,alpha


EmbeddingHK=LinOT.LinHKEmbedding(muRef,posRef,kappa)
EmbeddingHK.addSamples((tanDataV,tanDataAlpha))
EmbeddingHK.centerSamples()
EmbeddingHK.performPCA()

i=0
v=tanDataV[i]
alpha=tanDataAlpha[i]

muExp,posExp=LinOT.ExpHK(muRef,posRef,v,alpha,kappa)

img=aux.rasterizePointCloud2d(muExp,posExp,(64,64,))
plt.imshow(img)

np.sum(muExp)

np.einsum(v**2,[0,1],muRef,[0],[])+0.25*kappa**2*np.sum(alpha**2*muRef)

-np.sum(alpha*muRef)*kappa**2

Embedding=LinOT.LinSHKEmbedding(muRef,posRef,kappa)

vHat,alphaHat=Embedding.normalizeTan(v,alpha)

muExpHat,posExpHat=LinOT.ExpHK(muRef,posRef,vHat,alphaHat,kappa)

img=aux.rasterizePointCloud2d(muExp,posExp,(64,64,))
plt.imshow(img)

np.sum(muExpHat)

Embedding=LinOT.LinSHKEmbedding(muRef,posRef,kappa)
#tanDataSHK=Embedding.normalizeTan(tanDataV,tanDataAlpha)
#tanDataSHK=Embedding.tanHKToSHK(*tanDataSHK)
#Embedding.addSamples(tanDataSHK)
Embedding.addSamples((tanDataV,tanDataAlpha),fromHK=True)
Embedding.centerSamples()
Embedding.performPCA()

plt.plot(EmbeddingHK.pca_var,marker="x",lw=0)
plt.plot(Embedding.pca_var,marker="x",lw=0)
plt.show()

plt.scatter(Embedding.pca_coords[:,0],Embedding.pca_coords[:,1])
plt.scatter(EmbeddingHK.pca_coords[:,0],EmbeddingHK.pca_coords[:,1])
plt.show()

for i in [0,1,2]:
    print(i)
    vec=np.zeros(i+1)
    vec[i]=1.
    res=Embedding.expPCA(vec,tSeq=Embedding.pca_std[i]*np.linspace(-1,1,num=7))

    imgList=[aux.rasterizePointCloud2d(mu,pos,(64,64,)) for mu,pos in res]
    print(np.sum(imgList,axis=(1,2)))
    Vis.ShowImageArray(imgList,7,2,2,scaleMode="common")

for i in [0,1,2]:
    print(i)
    vec=np.zeros(i+1)
    vec[i]=1.
    res=EmbeddingHK.expPCA(vec,tSeq=Embedding.pca_std[i]*np.linspace(-1,1,num=7))

    imgList=[aux.rasterizePointCloud2d(mu,pos,(64,64,)) for mu,pos in res]
    print(np.sum(imgList,axis=(1,2)))
    Vis.ShowImageArray(imgList,7,2,2,scaleMode="common")

v,alpha=EmbeddingHK.convertPCAToRaw(np.array([0,0,1]),shiftMean=False)

np.einsum(v**2,[0,1],muRef,[0],[])

0.25*kappa**2*np.sum(alpha**2*muRef)

v,alpha=Embedding.convertPCAToRaw(np.array([0,0,1]),shiftMean=False)


