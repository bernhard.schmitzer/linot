# ---
# jupyter:
#   jupytext:
#     formats: py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.0
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
import numpy as np
import scipy
import scipy.linalg
import lib.LinOT.auxiliary as aux
import lib.LinOT.SinkhornNP as Sinkhorn
import auxlib.OTCommon as OTCommon

from lib.LinOT import *
import lib.LinOT.Visualization as Vis

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as cm
matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']
# -
A=np.array([[2,0.3],[0.3,1]])
np.linalg.eigh(A)

fig=plt.figure()
ax=fig.add_subplot(aspect=1.)
#addEllipse(ax,(0,0),1,2,angle=20)
Vis.addEllipseMat(ax,(0,0),A,fill=None,ec="k")
ax.set_xlim([-3,3])
ax.set_ylim([-3,3])

cov=np.identity(2)
mean=np.zeros(2)
Embedding=LinW2GaussEmbedding(cov,mean)

covs=np.array([
        [[2,0],[0,1]],
        [[1,0],[0,2]]
        ])
means=np.array([[1,0],[0,1]])

Embedding.addSamples((covs,means))

Embedding.centerSamples()

Embedding.performPCA()

Embedding.pca_std

# +
i=0
print(i)
vec=np.zeros(i+1)
vec[i]=1.
res=Embedding.expPCA(vec,tSeq=Embedding.pca_std[i]*np.linspace(-1,1,num=7))

fig=plt.figure()
ax=fig.add_subplot(aspect=1.)
for i,(cov,mean) in enumerate(res):
    Vis.addEllipseMat(ax,mean,cov,fill=None,ec=cm.viridis(i/6))
ax.set_xlim([-3,3])
ax.set_ylim([-3,3])
# -


