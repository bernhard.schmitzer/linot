# Examples-Doc
This directory contains examples of the various variants of linearized OT that are implemented in this library.

## Optimal transport on point clouds / images
* [001-MeasurePreprocessing-Sinkhorn-TangentVectorExtraction.ipynb](001-MeasurePreprocessing-Sinkhorn-TangentVectorExtraction.ipynb) generate some example images of deforming ellipses, dump them to file, compute linearized OT embeddings for Wasserstein-2 and Hellinger--Kantorovich metrics via entropic regularization, dump them to file
* [002-LinW2.ipynb](002-LinW2.ipynb) use above example images and pre-computed tangent vectors to perform a simple analysis in tangent space for linearized **Wasserstein-2**
* [005-LinHK.ipynb](005-LinHK.ipynb) as above, for linearized **Hellinger--Kantorovich**
* [006-LinSHK.ipynb](006-LinSHK.ipynb) as above, for linearized **spherical Hellinger--Kantorovich**

## Special cases of Wasserstein-2
* [003-LinW2-Gauss.ipynb](003-LinW2-Gauss.ipynb) Gaussian measures are a submanifold of the Wasserstein-2 pseudo-manifold and the LinW2 can be restricted to this setting, yielding many close-form expressions and essentially zero computational complexity. Often this approximation yields still very good results, that can be easier to interpret.
* [004-LinW2-1d.ipynb](004-LinW2-1d.ipynb) In one dimension, on the real line, the W2 metric becomes flat and can be expressed as a norm in a L2 Hilbert space, based on the inverse cumulative distribution function. Again, this yields greatly simplified anlaysis and numerics.
