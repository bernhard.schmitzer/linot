# ---
# jupyter:
#   jupytext:
#     formats: py:light,ipynb
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# can be removed if LinOT package is installed via pip
import sys
sys.path.append("../../lib")

import numpy as np

import LinOT
import LinOT.Visualization as Vis
import LinOT.auxiliary as aux

import matplotlib
import matplotlib.pyplot as plt

matplotlib.rc('image', interpolation='nearest')
matplotlib.rc('figure',facecolor='white')
matplotlib.rc('image',cmap='viridis')

prop_cycle = plt.rcParams['axes.prop_cycle']
colors = prop_cycle.by_key()['color']

# +
# specify filenames for samples and tangent data
tag="ex1_single-ellipses"
subtag=""
filenameCenter="data/"+tag+"/center_data"+subtag+".npz"
filenameTan="data/"+tag+"/tan_data"+subtag+"_HK_kappa32.npz"

dat=np.load(filenameTan)
tanDataV=dat["tanListV"]
tanDataAlpha=dat["tanListAlpha"]
imgCenter=np.load(filenameCenter)["img"]

muRef,posRef=aux.processDensity_Grid(imgCenter,\
        totalMass=1.,keepZero=False)

kappa=32.
# -

# create embedding object, add samples, center samples, perform pca
EmbeddingHK=LinOT.LinHKEmbedding(muRef,posRef,kappa=kappa)
EmbeddingHK.addSamples((tanDataV,tanDataAlpha))
EmbeddingHK.centerSamples()
EmbeddingHK.performPCA()
# same for SHK
EmbeddingSHK=LinOT.LinSHKEmbedding(muRef,posRef,kappa=kappa)
EmbeddingSHK.addSamples((tanDataV,tanDataAlpha),fromHK=True)
EmbeddingSHK.centerSamples()
EmbeddingSHK.performPCA()

fig=plt.figure()
ax=fig.add_subplot()
ax.set_ylim(1E-4,1E2)
Vis.PCASpectrum(EmbeddingHK,ax=ax,label="HK")
Vis.PCASpectrum(EmbeddingSHK,ax=ax,label="SHK")
plt.legend()
plt.tight_layout()
plt.show()

fig=plt.figure()
ax=fig.add_subplot()
Vis.PCAPlot(EmbeddingHK,ax=ax,label="HK")
Vis.PCAPlot(EmbeddingSHK,ax=ax,label="SHK")
plt.legend()
plt.show()


# +
# compare means
resHK=EmbeddingHK.getMeanExp()
imgHK=aux.rasterizePointCloud2d(*resHK,(64,64,))

resSHK=EmbeddingSHK.getMeanExp()
imgSHK=aux.rasterizePointCloud2d(*resSHK,(64,64,))

print("mass HK:",np.sum(resHK[0]))
print("mass SHK:",np.sum(resSHK[0]))

Vis.ShowImageArray([imgHK,imgSHK],2,3,3,scaleMode="common")
# -

for i in [0,1,2]:
    print(i)
    vec=np.zeros(i+1)
    vec[i]=1.
    res=EmbeddingHK.expPCA(vec,tSeq=EmbeddingHK.pca_std[i]*np.linspace(-1,1,num=7))

    imgList=[aux.rasterizePointCloud2d(mu,pos,(64,64,)) for mu,pos in res]
    print(np.sum(imgList,axis=(1,2)))
    Vis.ShowImageArray(imgList,7,2,2,scaleMode="common")

for i in [0,1,2]:
    print(i)
    vec=np.zeros(i+1)
    vec[i]=1.
    res=EmbeddingSHK.expPCA(vec,tSeq=EmbeddingSHK.pca_std[i]*np.linspace(-1,1,num=7))

    imgList=[aux.rasterizePointCloud2d(mu,pos,(64,64,)) for mu,pos in res]
    Vis.ShowImageArray(imgList,7,2,2,scaleMode="common")


