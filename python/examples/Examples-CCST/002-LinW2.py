# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.5
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# +
# can be removed if LinOT package is installed via pip
import sys
sys.path.append('../../lib')

import numpy as np

import LinOT
import LinOT.auxiliary as aux
import LinOT.SinkhornNP as Sinkhorn
# -

# specify filenames for samples and tangent data
tag = "two_ellipses_rad0d25/"
subtag = "W2"
filenameSamples = "data/" + tag + "sample_data.npz"
filenameCenter = "data/" + tag + "center_data" + subtag + ".npz"
filenameTan = "data/" + tag + "tan_data" + subtag + ".npz"

# # Compute Wasserstein-2 tangent space embedding

# load reference measure, here we just take Euclidean mean
datRef = aux.importMeasure("data/" + tag + "mean.mat", totalMass=1., keepZero=False)
muRef = datRef[1]
posRef = datRef[2].astype(np.double)

params = {}
params["setup_HKMode"] = False
params["solver_errorGoal"] = 1.E-3
params["aux_verbose"] = False
params["solver_epsStart"] = 1E3
params["solver_epsTarget"] = 0.1

# compute all tangent vectors and distances
tanList = []
distList = []
for i in range(64):
    print(i)
    filename = "data/" + tag + "sample_{:03d}.mat".format(i)
    datSamp = aux.importMeasure(filename, totalMass=1., keepZero=False)
    muSamp = datSamp[1]
    posSamp = datSamp[2].astype(np.double)

    # solve W transport
    value, pi = Sinkhorn.SolveW2(muRef, posRef, muSamp, posSamp,
                                 SinkhornError=params["solver_errorGoal"],
                                 epsTarget=params["solver_epsTarget"], epsInit=params["solver_epsStart"],
                                 returnSolver=False
                                 )
    # extract approximate Monge map (which is logarithmic map, up to subtracting initial locations)
    v = LinOT.LogW2(pi, posRef, posSamp)
    tanList.append(v)
    distList.append(value)

np.savez_compressed(filenameTan, tanList=tanList, distList=distList)


