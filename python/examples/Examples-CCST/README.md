# Examples-CCST
This directory contains some of the examples presented in the [Cai et al.](https://doi.org/10.48550/arXiv.2102.08807) for linearized W2 and HK. Code for these examples was originally published on https://github.com/bernhard-schmitzer/UnbalancedLOT. The code here is adjusted to use the new LinOT library (which includes a slightly updated definition of the barycentric projection for the HK distance).

## Outline of directory
* data contains the samples for the first example in the paper, ellipses of varying elongation and size. As reference measure the linear mean is prepared. Precomputed W2 and HK tangent vectors are already prepared, but can also be re-computed with the given code.
* [001-Compute-Pairwise-Transport-ExpMaps.ipynb](001-Compute-Pairwise-Transport-ExpMaps.ipynb) demonstrates how the (un-)balanced optimal transport problem between two measures can be solved, how the linear embeddings are obtained from the optimal plans, and a simple example on how to use the exponential map
* [002-LinW2.ipynb](002-LinW2.ipynb) computes the linear embeddings for all samples for the W2 metric and dumps them to a file for further analysis (file already pre-computed and contained in repository)
* [003-LinHK.ipynb](003-LinHK.ipynb) as above but for the Hellinger--Kantorovich metric
* [004-TangentSpaceAnalysis.ipynb](004-TangentSpaceAnalysis.ipynb) provides a simple PCA analysis in tangent space as well as rudimentary visualizations as given in the paper
