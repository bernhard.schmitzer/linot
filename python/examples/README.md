# Examples
Examples are split into three groups.

## Examples-Doc
This directory provides examples that essentially serve as preliminary documentation of the scope of the library, demonstrating (almost) all features.
This includes linearized W2, HK, SHK, as well as the special cases of linearized W2 for Gaussian measures and in one dimension.
A notable exception, which is not shown, is the analysis of measures on manifolds. For this see the directory Examples-SaSc.

**[Go to Examples-Doc](Examples-Doc)**

## Examples-CCST
This directory contains some of the examples presented in the [Cai et al.](https://doi.org/10.48550/arXiv.2102.08807) for linearized W2 and HK. The code is adjusted to use the new LinOT library (which includes a slightly updated definition of the barycentric projection for the HK distance).

**[Go to Examples-CCST](Examples-CCST)**

## Examples-SaSc
This directory contains some of the examples presented in [Sarrazin and Schmitzer](https://arxiv.org/abs/2303.13901). This includes linearized HK and SHK distances, as well as an example of lineairzed optimal transport with measures on a sphere.

**[Go to Examples-SaSc](Examples-SaSc)**
