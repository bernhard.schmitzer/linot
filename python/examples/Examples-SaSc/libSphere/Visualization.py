# ---
# jupyter:
#   jupytext:
#     formats: py:light
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import numpy as np


# -

def drawX(ax, rad, alpha=.1):
    u, v = np.mgrid[0:2*np.pi:50j, 0:np.pi:50j]
    x = rad*np.cos(u)*np.sin(v)
    y = rad*np.sin(u)*np.sin(v)
    z = rad*np.cos(v)
    ax.plot_surface(x, y, z, color="b", alpha=.1)


def generateSphere(rad):
    u, v = np.mgrid[0:2*np.pi:50j, 0:np.pi:50j]
    x = rad*np.cos(u)*np.sin(v)
    y = rad*np.sin(u)*np.sin(v)
    z = rad*np.cos(v)
    return x,y,z

def plotSphere(ax,rad,alpha=.1):
    u, v = np.mgrid[0:2*np.pi:50j, 0:np.pi:50j]
    x = rad*np.cos(u)*np.sin(v)
    y = rad*np.sin(u)*np.sin(v)
    z = rad*np.cos(v)
    ax.plot_surface(x, y, z, color="b", alpha=alpha)


# -
def sampleFromSphere(n):
    # https://stackoverflow.com/a/26127012
    samples=n
    points = []
    phi = np.pi * (3. - np.sqrt(5.))  # golden angle in radians

    for i in range(samples):
        y = 1 - (i / (samples - 1)) * 2  # y goes from 1 to -1
        radius = np.sqrt(1 - y * y)  # radius at y

        theta = phi * i  # golden angle increment

        x = np.cos(theta) * radius
        z = np.sin(theta) * radius

        points.append((x, y, z))

    return np.array(points)

