# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.5'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

import numpy as np

# +
#Exponential and logarithmic maps on the sphere, for future use.


def Norm(z):
    return np.linalg.norm(z, axis=-1)


def expX(x, v, rad):
    dim=x.shape[-1]
    pts=x.reshape(-1,dim)
    vecs=v.reshape(-1,dim)
    nrv=Norm(vecs)
    sinc=np.where(nrv>1e-10,np.sin(nrv/rad)/(np.maximum(nrv,1e-10)),1./rad)
    posExp=np.einsum("i,ij->ij",np.cos(nrv/rad),pts)+rad*np.einsum("i,ij->ij",sinc,vecs)
    return np.squeeze(posExp)
    

def logX(x,y, rad, cross=False):
    dim=x.shape[-1]
    ptsX,ptsY=x.reshape(-1,dim),y.reshape(-1,dim)
    nPtsX,nPtsY=ptsX.shape[0],ptsY.shape[0]
    if cross:
        scaMat=np.einsum("ik,jk->ij",ptsX,ptsY)/rad**2
        pOrth=ptsY.reshape((1,nPtsY,dim))-np.einsum("ij,ik->ijk",scaMat,ptsX)
        nOrth=Norm(pOrth).reshape(nPtsX,nPtsY,1)
        pOrth=np.where(nOrth>1e-15,np.einsum("ijk,ijl->ijk",pOrth,1/np.maximum(nOrth,1e-15)),0)
        scaMat=np.minimum(np.maximum(scaMat,-1),1)
        pOrth[scaMat+1<1e-15]=[0,0,1]
        return rad*np.einsum("ij,ijk->ijk",np.arccos(scaMat),pOrth)
    else:
        assert(ptsX.shape[0]==ptsY.shape[0])
        scaMat=np.einsum("ik,ik->i",ptsX,ptsY)/rad**2
        pOrth=ptsY-np.einsum("i,ik->ik",scaMat,ptsX)
        nOrth=Norm(pOrth)
        pOrth=np.einsum("ik,i->ik",pOrth,1/nOrth)
        return rad*np.einsum("i,ik->ik",np.arccos(scaMat),pOrth)

def dX(x,y,rad,cross=False):
    vecs=logX(x,y,rad,cross)
    return Norm(vecs)