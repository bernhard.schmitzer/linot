import numpy as np
import scipy
import scipy.io as sciio

def importImage(filename,totalMass=1.,keepZero=False):
    dat=np.load(filename)
    key=list(dat.keys())[0]
    img=dat[key]
    if img.ndim==2:
        muRef,posRef=processDensity_Grid(img,\
                totalMass=totalMass,keepZero=keepZero)
        return muRef,posRef
    elif img.ndim==3:
        sampleDat=[processDensity_Grid(img,totalMass=totalMass,keepZero=keepZero)
                for img in img]
        return sampleDat


